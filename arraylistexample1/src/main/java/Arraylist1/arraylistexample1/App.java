package Arraylist1.arraylistexample1;

import java.util.ArrayList;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
    	ArrayList<String> obj = new ArrayList<String>();
    	
    	obj.add("Ajeet");
    	obj.add("Harry");
    	obj.add("Chaitanya");
    	obj.add("Steve");
    	obj.add("Anuj");
    	
    	System.out.println("Currently the arraylist has the following elements "+obj);
    	
    	obj.add(0,"Rahul");
    	obj.add(1,"keyul");
    	System.out.println("Currently the arraylist has the following elements "+obj);
    	obj.remove("Rahul");
    	obj.remove("Anuj");
    	System.out.println("Currently the arraylist has the following elements "+obj);
    	obj.remove(1);
    	obj.remove(2);
    	System.out.println("Currently the arraylist has the following elements "+obj);
   }
}
