package com.hibernate.demoEntity;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

@Entity
@Table(name="STUDENT_DETAILS")
public class StudentDetailOnetoOneDemo {
	@Id @GeneratedValue(generator = "newGenerator") // name of the primary key Generator
	@GenericGenerator(name = "newGenerator", strategy = "increment",parameters = {@Parameter(name = "Student", value = "property")})
	private int id;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		id = id;
	}
	


//	@OneToOne(cascade = CascadeType.ALL)
//	@JoinColumn(name = "id")
//	private StudentGeneratedValueDemo StudentGeneratedValueDemo;

	
	String phonenumber;

	public String getPhonenumber() {
		return phonenumber;
	}
	public void setPhonenumber(String phonenumber) {
		this.phonenumber = phonenumber;
	}
	
//	public StudentGeneratedValueDemo getStudentGeneratedValueDemo() {
//		return StudentGeneratedValueDemo;
//	}
//	public void setStudentGeneratedValueDemo(StudentGeneratedValueDemo studentGeneratedValueDemo) {
//		StudentGeneratedValueDemo = studentGeneratedValueDemo;
//	}
	

}
