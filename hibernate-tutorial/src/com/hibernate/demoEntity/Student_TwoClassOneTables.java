package com.hibernate.demoEntity;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SecondaryTable;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


/*  There are two ways we can use this model class to interact with the database in Hibernate 
1 Annotations
2 XML

1 Annotations - @entity- tells hibernate that the class in an entity that is a class written to hold or persist or hold data into the database 
				@id - it is to specify the primary key
				@Table - By writing this annotation we are telling the hibernate whenever this object is passed for saving or deleting 
				the record or performing any other database operations , just consider the Student_information table
				In general the data must be written 
       */

@Entity
@Table(name = "student")
@SecondaryTable(name ="Student_details")
public class Student_TwoClassOneTables {
	public Student_TwoClassOneTables()
	{}
	@Id

	@Column(name="id") 	
	@GeneratedValue
    private int id;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	
	
	@Column(name="firstname")
	private String firstname;
	
	@Column(name="lastname")
	private String lastname;
	


	@Embedded 
	public StudentDetail_TwoClassOnetable getStudentDetail_TwoClassOnetable() {
		return StudentDetail_TwoClassOnetable;
	}
	public void setStudentDetail_TwoClassOnetable(StudentDetail_TwoClassOnetable studentDetail_TwoClassOnetable) {
		StudentDetail_TwoClassOnetable = studentDetail_TwoClassOnetable;
	}
	private StudentDetail_TwoClassOnetable StudentDetail_TwoClassOnetable;
	
	
	public Student_TwoClassOneTables(String firstname, String lastname) {
		this.firstname = firstname;
		this.lastname = lastname;
	}
	@Override
	public String toString() {
		return "Student [id=" + id + ", firstname=" + firstname + ", lastname=" + lastname + "]";
	}
	

}
