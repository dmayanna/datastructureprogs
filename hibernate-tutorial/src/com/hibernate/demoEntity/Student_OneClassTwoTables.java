package com.hibernate.demoEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SecondaryTable;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


/*  There are two ways we can use this model class to interact with the database in Hibernate 
1 Annotations
2 XML

1 Annotations - @entity- tells hibernate that the class in an entity that is a class written to hold or persist or hold data into the database 
				@id - it is to specify the primary key
				@Table - By writing this annotation we are telling the hibernate whenever this object is passed for saving or deleting 
				the record or performing any other database operations , just consider the Student_information table
				In general the data must be written 
       */

@Entity
@Table(name = "student")
@SecondaryTable(name ="Student_details")
public class Student_OneClassTwoTables {
	public Student_OneClassTwoTables()
	{}
	@Id

	@Column(name="id") 	
	@GeneratedValue
    private int id;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	@Column(name="firstname")
	private String firstname;
	
	@Column(name="lastname")
	private String lastname;
	
	@Column(name="email")
	private String email;
	
	@Column(table="Student_details")
    public String getPhoneNumber() {
		return PhoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		PhoneNumber = phoneNumber;
	}
	@Column(table="Student_details",name="PhoneNumber")
	private String PhoneNumber;
	
	
	public Student_OneClassTwoTables(String firstname, String lastname, String email) {
		this.firstname = firstname;
		this.lastname = lastname;
		this.email = email;
	}
	@Override
	public String toString() {
		return "Student [id=" + id + ", firstname=" + firstname + ", lastname=" + lastname + ", email=" + email + "]";
	}
	

}
