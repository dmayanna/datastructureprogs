package com.hibernate.demoEntity;

import java.util.List;

import javax.persistence.OneToMany;

public class College_oneToMany {

	public int getCollegeId() {
		return collegeId;
	}
	public void setCollegeId(int collegeId) {
		this.collegeId = collegeId;
	}
	public String getCollegeName() {
		return CollegeName;
	}
	public void setCollegeName(String collegeName) {
		CollegeName = collegeName;
	}
//	@OneToMany(targetEntity = Student_OneToMany.class, mappedBy="college", cascade=Ca)
	public List<Student> getStudents() {
		return students;
	}
	public void setStudents(List<Student> students) {
		this.students = students;
	}
	private int collegeId;
	private String CollegeName;
	private List<Student> students;
}
