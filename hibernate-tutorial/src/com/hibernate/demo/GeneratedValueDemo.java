package com.hibernate.demo;

import javax.persistence.GeneratedValue;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.hibernate.demoEntity.Student;
import com.hibernate.demoEntity.StudentGeneratedValueDemo;
/*
 * This class combined with class StudentGeneratedValueDemo.java is programmed to 
 * demonstarate @GeneratedValue annotation in Hibernate
 * @GeneratedValue is used to increment the primary key value of the Database table assuming the value that 
 * goes into that column is a sequence 
 * In order to use this we have to create a sequence in the database, atleast thats what i did to successfully implement this. 
 * create sequence hibernate_sequence
 * 
 */

public class GeneratedValueDemo {

	public static void main(String[] args) {

		// create session factory
		SessionFactory factory = new Configuration()
								.configure("hibernate.cfg.xml")
								.addAnnotatedClass(StudentGeneratedValueDemo.class)
								.buildSessionFactory();
		
		// create session
		Session session = factory.getCurrentSession();
		
		try {			
			// create a student object
			System.out.println("Creating new student object...");
			StudentGeneratedValueDemo tempStudent = new StudentGeneratedValueDemo("Generated", "value", "smdeekshith@gmail.com");
			
			// start a transaction
			session.beginTransaction();
			
			// save the student object
			System.out.println("Saving the student...");
			session.save(tempStudent);
			
			// commit transaction
			session.getTransaction().commit();
			
			System.out.println("Done!");
		}
		finally {
			factory.close();
		}
	}

}





