package com.hibernate.demo;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.hibernate.demoEntity.Student;


public class readStudentDemo {

	public static void main(String[] args) {

		// create session factory
		SessionFactory factory = new Configuration()
								.configure("hibernate.cfg.xml")
								.addAnnotatedClass(Student.class)
								.buildSessionFactory();
		
		// create session
		Session session = factory.getCurrentSession();
		
		try {			
			// create a student object
			System.out.println("Creating new student object...");
			Student tempStudent = new Student("Daffy", "duck", "daffyduck@gmail.com");
			
			// start a transaction
			session.beginTransaction();
			
			// save the student object
			System.out.println("Saving the student...");
			session.save(tempStudent);
			
			// commit transaction
			session.getTransaction().commit();
			
			System.out.println("Done inserting the data or Object!");
			
			// find out the students id : Primary Key
			System.out.println("Saved student generated id : "+ tempStudent.getId());
			
			// Now get a new session and start transaction 
			session = factory.getCurrentSession();
			session.beginTransaction();
			
			// retrieve student based on the id: Primary key  
			System.out.println("\n Getting Student with Id "+tempStudent.getId());
			Student mystudent = (Student) session.get(Student.class,tempStudent.getId());
			System.out.println("Get Complete : "+mystudent);
			// commit the transaction
			session.getTransaction().commit();
			System.out.println("Done ..!!");
		}
		finally {
			factory.close();
		}
	}

}





