package com.hibernate.demo;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.tool.hbm2ddl.SchemaExport;

import com.hibernate.demoEntity.Student;
import com.hibernate.demoEntity.StudentDetail_TwoClassOnetable;
import com.hibernate.demoEntity.Student_OneClassTwoTables;
import com.hibernate.demoEntity.Student_TwoClassOneTables;


public class TwoClassOneTables {

	public static void main(String[] args) {

		// create session factory
		SessionFactory factory = new Configuration()
								.configure("hibernate.cfg.xml")
								.addAnnotatedClass(Student_TwoClassOneTables.class).addAnnotatedClass(StudentDetail_TwoClassOnetable.class)
								.buildSessionFactory();
		
		// create session
		Session session = factory.getCurrentSession();
		
		try {			
			// create a student object
			System.out.println("Creating new student object...");
			
			
			StudentDetail_TwoClassOnetable StuDet = new StudentDetail_TwoClassOnetable();
			StuDet.setEmail("Smd@dsd.com");
			Student_TwoClassOneTables tempStudent = new Student_TwoClassOneTables("Reekshith", "Mayanna");
			tempStudent.setStudentDetail_TwoClassOnetable(StuDet);
			
			// start a transaction
			session.beginTransaction();
			
			// save the student object
			System.out.println("Saving the student...");
			session.save(tempStudent);
			
			// commit transaction
			session.getTransaction().commit();
			
			System.out.println("Done!");
		}
		finally {
			factory.close();
		}
	}

}





