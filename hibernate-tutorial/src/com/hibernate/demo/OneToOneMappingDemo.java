package com.hibernate.demo;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;
import org.hibernate.cfg.Configuration;

import com.hibernate.demoEntity.Student;
import com.hibernate.demoEntity.StudentDetailOnetoOneDemo;
import com.hibernate.demoEntity.StudentGeneratedValueDemo;
import com.hibernate.demoEntity.Student_onetoonedemo;

/*
 * one to one mapping
 * each row in table A is linked to each row in table B
 * Number of rows in A is equal to number of rows in B
 * one to one relationship is established using one to one relationship
 * Classes used for this program
 * OneToOneMappingDemo
 * Student_onetoonedemo
 * StudentDetailOnetoOneDemo
 * 
 * 
 * Error - could not instantiate id generator in StudentDetailsonetoone class 
 * Soln - 	@GenericGenerator(name = "newGenerator", strategy = "increment",parameters = {@Parameter(name = "Student", value = "property")})
 * Changed strategy here from foreign to increment
 * 
 */

public class OneToOneMappingDemo {

	public static void main(String[] args) {

		// create session factory
		SessionFactory factory = new Configuration()
								.configure("hibernate.cfg.xml")
								.addAnnotatedClass(Student_onetoonedemo.class).addAnnotatedClass(StudentDetailOnetoOneDemo.class)
								.buildSessionFactory();
		
		// create session
		Session session = factory.getCurrentSession();
		
		try {			
			// create a student object
			System.out.println("Creating new student object...");
			Student_onetoonedemo tempStudent = new Student_onetoonedemo("Djeek", "Mayanjna", "smdeejbkshith@gmail.com");
			StudentDetailOnetoOneDemo StudentDet = new StudentDetailOnetoOneDemo();
			StudentDet.setPhonenumber("988444298036");
			tempStudent.setStudentDet(StudentDet);
			// start a transaction
			session.beginTransaction();
			
			// save the student object
			System.out.println("Saving the student...");
			session.save(tempStudent);
			
			// commit transaction
			session.getTransaction().commit();
			
			System.out.println("Done!");
		}
		finally {
			factory.close();
		}
	}

}





