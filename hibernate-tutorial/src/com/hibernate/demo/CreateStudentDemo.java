package com.hibernate.demo;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.hibernate.demoEntity.Student;
//This class in combination with student class 
//and createStudent will insert a student detail into the database 

public class CreateStudentDemo {

	public static void main(String[] args) {

		// create session factory
		@SuppressWarnings("deprecation")
		SessionFactory factory = new Configuration()
								.configure("hibernate.cfg.xml")
								.addAnnotatedClass(Student.class)
								.buildSessionFactory();
		
		// create session
		Session session = factory.getCurrentSession();
		
					
			// create a student object
			System.out.println("Creating new student object...");
			Student tempStudent = new Student("Deekshith", "Mayanna", "smdeekshith@gmail.com");
			
			// start a transaction
			session.beginTransaction();
			
			// save the student object
			System.out.println("Saving the student...");
			session.save(tempStudent);
			
			// commit transaction
			session.getTransaction().commit();
			
			System.out.println("Done!");
			
			tempStudent=null;
			
			session = factory.openSession();
			session.beginTransaction();
			tempStudent= (Student) session.get(Student.class, 1);
			System.out.println(" The Student name is "+tempStudent.getFirstname());
			session.close();
			factory.close();
		}
	}







