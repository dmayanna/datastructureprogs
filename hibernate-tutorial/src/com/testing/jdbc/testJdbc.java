package com.testing.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class testJdbc {
	
	public static void main(String[] args)
	{
	

		
			try {
				System.out.println("Checking for JDBC Driver class");
				Class.forName("oracle.jdbc.driver.OracleDriver");
				
			} 
			catch (ClassNotFoundException e) {
				System.out.println("Where is your Oracle JDBC Driver?");
				e.printStackTrace();
			}
			try {
				String jdbcUrl = "jdbc:oracle:thin:@localhost:1521/orcl";
				String user ="hr";
				String password="oracle";
				Connection connection = DriverManager.getConnection(jdbcUrl,user,password );
		     	System.out.println("Connection successfull");
				
		} catch (SQLException e) 
			{
				e.printStackTrace();
			}
			
				
			
		}

}
