package com.deekshith.model;

import java.io.Serializable;
import java.util.List;

@SuppressWarnings("serial")
public class WwePlayers implements Serializable {
	private String name;
	private Double biceps;
	private float chest;
	private Double height;
	private int rank;
	List<String> titles;
	WwePlayerInfo playerInfo;

	public WwePlayers(String name, Double biceps, float chest, Double height, List<String> titles, int rank,
			WwePlayerInfo playerInfo) {
		super();
		this.name = name;
		this.biceps = biceps;
		this.chest = chest;
		this.height = height;
		this.titles = titles;
		this.rank = rank;
		this.playerInfo = playerInfo;
	}
	
	public WwePlayers() {
	}

	public String getName() {
		return name;
	}

	public Double getBiceps() {
		return biceps;
	}

	public float getchest() {
		return chest;
	}

	public Double getHeight() {
		return height;
	}

	public int getRank() {
		return rank;
	}

	public void setTitles(List<String> titles) {
		this.titles = titles;
	}

	public List<String> getTitles() {
		return titles;
	}

	public WwePlayerInfo getPlayerInfo() {
		return playerInfo;
	}

	public void setPlayerInfo(WwePlayerInfo playerInfo) {
		this.playerInfo = playerInfo;
	}

	@Override
	public String toString() {
		return "WwePlayers [name=" + name + ", biceps=" + biceps + ", chest=" + chest + ", height=" + height
				+ ", titles=" + titles + "]";
	}

}
