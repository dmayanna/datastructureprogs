package com.deekshith.model;

import java.io.Serializable;
import java.util.List;

@SuppressWarnings("serial")
public class WwePlayerInfo implements Serializable {
private String stageName;
private String realName;
private float phoneNumber;
private List<String> moviesActed;

public WwePlayerInfo(String stageName, String realName, int phoneNumber, List<String> moviesActed) {
	super();
	this.stageName = stageName;
	this.realName = realName;
	this.phoneNumber = phoneNumber;
	this.moviesActed = moviesActed;
}
public WwePlayerInfo() {
}
public String getStageName() {
	return stageName;
}
public void setStageName(String stageName) {
	this.stageName = stageName;
}
public String getRealName() {
	return realName;
}
public void setRealName(String realName) {
	this.realName = realName;
}
public float getPhoneNumber() {
	return phoneNumber;
}
public void setPhoneNumber(int phoneNumber) {
	this.phoneNumber = phoneNumber;
}
public List<String> getMoviesActed() {
	return moviesActed;
}
public void setMoviesActed(List<String> moviesActed) {
	this.moviesActed = moviesActed;
}
 
}
