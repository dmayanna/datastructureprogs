package com.deekshith.jsonExPrograms.JsonPrograms;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.deekshith.model.WwePlayerInfo;
import com.deekshith.model.WwePlayers;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class JavaToJsonConversion {
	@SuppressWarnings("unchecked")
	public static void main(String[] args) {
		@SuppressWarnings("rawtypes")
		List titles = new ArrayList<String>();
		titles.add("Intercontinental champion");
		titles.add("Royal Rumble Champion");
		titles.add("King of the Ring");
		List<String> moviesActed = new ArrayList<String>();
		moviesActed.add("movie 1");
		moviesActed.add("movie 2");
		moviesActed.add("movie 3");
		WwePlayerInfo playerinfo = new WwePlayerInfo("The Rock", "Dwayne Johnson", 433434532, moviesActed);
		WwePlayers rock = new WwePlayers("The Rock", 22.0, 52, 6.2, titles, 3, playerinfo);
		ObjectMapper mapper = new ObjectMapper();
		try {
			String jsonInString = mapper.writeValueAsString(rock);
			System.out.println(jsonInString);
			mapper.writeValue(new File("playerJson.json"), rock);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
