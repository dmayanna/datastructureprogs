package com.deekshith.jsonExPrograms.JsonPrograms;

import java.io.File;
import java.io.IOException;

import com.deekshith.model.WwePlayers;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class JsontoJavaConversion {

	public static void main(String[] args) {
		ObjectMapper mapper = new ObjectMapper();
		try {
			WwePlayers rock = mapper.readValue(new File("playerJson.json"), WwePlayers.class);
			System.out.println(rock.toString());
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
