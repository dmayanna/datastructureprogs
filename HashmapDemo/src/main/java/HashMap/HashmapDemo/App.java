package HashMap.HashmapDemo;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.TreeMap;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
    	HashMap<Integer,String> hmap = new HashMap<Integer,String>();
    	hmap.put(6, "value");
    	hmap.put(34, "something");
    	hmap.put(3, "values");
    	hmap.put(7, "Deekshith");
    	hmap.put(154, "valweruehjg");
    	hmap.put(14, "df");
    	hmap.put(1, "cvvc");
    	hmap.put(34, "mmnmnmn");
    	
    	System.out.println(hmap);
     	System.out.println("----------------------------------------HashMap----------------------------------------------");

    	for(int k:hmap.keySet())
    	{
    	 System.out.println("Key is : "+k+" and value is : "+hmap.get(k));
    	}
    	System.out.println("Removing 3");
    	hmap.remove(3);
    	
     	System.out.println("----------------------------------------HashMapafterremoval----------------------------------------------");

    	
    	for(int k:hmap.keySet())
    	{
    	 System.out.println("Key is : "+k+" and value is : "+hmap.get(k));
    	}
    	
 	System.out.println("----------------------------------------LinkedHashMap----------------------------------------------");

 	
    	
    	LinkedHashMap<Integer,String> LinkedHashMap = new LinkedHashMap<Integer,String>();
    	LinkedHashMap.putAll(hmap);
    	for(int k:LinkedHashMap.keySet())
    	{
    	 System.out.println("Key is : "+k+" and value is : "+LinkedHashMap.get(k));
    	}
    	
     	System.out.println("KeySet : "+LinkedHashMap.keySet());

    	
    	System.out.println("----------------------------------------treeMap----------------------------------------------");
    	TreeMap<Integer,String> treeMap = new TreeMap<Integer,String>();
    	treeMap.putAll(hmap);
    	for(int k:treeMap.keySet())
    	{
    	 System.out.println("Key is : "+k+" and value is : "+treeMap.get(k));
    	}
   
    }
}
