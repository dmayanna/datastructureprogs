package ListsComparasion.Lists;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Vector;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
    	long n= (long)10E6;
    	long millis = System.currentTimeMillis();
    	
    	
    	
    	System.out.println(" Performing Insertion operation");
    	
    	
    	
    	ArrayList<String> arraylist = new ArrayList<String>();
    	for(int i=0;i<n;i++)
    	{
    		arraylist.add("Deekshith");
    	}
    	System.out.println("The time taken for the arraylist to perform the operation is "+(System.currentTimeMillis()-millis));
    	
    	
    	
    	
    	LinkedList<String> LinkedList = new LinkedList<String>();
    	millis = System.currentTimeMillis();
    	for(int i=0;i<n;i++)
    	{
    		LinkedList.add("Deekshith");
    	}
    	System.out.println("The time taken for the LinkedList to perform the operation is "+(System.currentTimeMillis()-millis));
    	
    	
    	
    	
    	Vector<String> Vector = new Vector<String>();
     	millis = System.currentTimeMillis();
    	for(int i=0;i<n;i++)
    	{
    		Vector.add("Deekshith");
    	}
    	System.out.println("The time taken for the Vector to perform the operation is "+(System.currentTimeMillis()-millis));
    	
    	
    	
    	
    	System.out.println(" Performing Search operation");
    	millis = System.currentTimeMillis();
    	for(int i=0;i<100;i++)
    	{
    	arraylist.get((int) 10000);
    	}
    	System.out.println("Search time for arraylist to perform the operation is "+((System.currentTimeMillis()-millis)));

    	
    	
    	
    	millis = System.currentTimeMillis();
    	for(int i=0;i<100;i++)
    	{
    	LinkedList.get((int) 10000);
    	}
    	System.out.println("Search time for LinkedList to perform the operation is "+((System.currentTimeMillis()-millis)));

    	
    	
    	
    	
    	millis = System.currentTimeMillis();
    	for(int i=0;i<100;i++)
    	{
    	Vector.get((int) (10000));
    	}
    	System.out.println("Search time for Vector to perform the operation is "+((System.currentTimeMillis()-millis)));
    	
    	
    	
    	
    	millis = System.currentTimeMillis();

    	System.out.println("Performing Deletion operation");
    	for(int i=(int) n-1;i>0;i--)
    	{
    		arraylist.remove(i);
    	}
    	System.out.println("Deletion operation of Arraylist is :"+(System.currentTimeMillis()-millis));

    	
    	
    	
    	millis = System.currentTimeMillis();

    	for(int i=(int) n-1;i>0;i--)
    	{
    		LinkedList.remove(i);
    	}
    	System.out.println("Deletion operation of LinkedList is :"+(System.currentTimeMillis()-millis));
    	
    	millis = System.currentTimeMillis();

    	for(int i=(int) n-1;i>0;i--)
    	{
    		Vector.remove(i);
    	}
    	System.out.println("Deletion operation of Vector is :"+(System.currentTimeMillis()-millis));  	

    	
    	
    }
}
