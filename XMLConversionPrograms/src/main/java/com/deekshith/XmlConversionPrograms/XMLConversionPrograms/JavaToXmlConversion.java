package com.deekshith.XmlConversionPrograms.XMLConversionPrograms;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import com.deekshith.model.WwePlayerInfo;
import com.deekshith.model.WwePlayers;

@SuppressWarnings("restriction")
public class JavaToXmlConversion {
	@SuppressWarnings("unchecked")
	public static void main(String[] args) {
		@SuppressWarnings("rawtypes")
		List titles = new ArrayList<String>();
		titles.add("Intercontinental champion");
		titles.add("Royal Rumble Champion");
		titles.add("King of the Ring");
		List<String> moviesActed = new ArrayList<String>();
		moviesActed.add("movie 1");
		moviesActed.add("movie 2");
		moviesActed.add("movie 3");
		WwePlayerInfo playerinfo = new WwePlayerInfo("The Rock", "Dwayne Johnson", 433434532, moviesActed);
		WwePlayers rock = new WwePlayers("The Rock", 22.0, 52, 6.2, titles, 3, playerinfo);
		try {
			File file = new File("file.xml");
			JAXBContext jaxbContext = JAXBContext.newInstance(WwePlayers.class);
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
			// output pretty printed
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			jaxbMarshaller.marshal(rock, file);
			jaxbMarshaller.marshal(rock, System.out);
		} catch (JAXBException e) {
			e.printStackTrace();
		}

	}
}
