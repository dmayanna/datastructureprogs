package com.deekshith.XmlConversionPrograms.XMLConversionPrograms;

import java.io.File;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import com.deekshith.model.WwePlayers;

@SuppressWarnings("restriction")
public class XmlToJavaConversion {
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		try {
			File file = new File("file.xml");
			JAXBContext jaxbContext = JAXBContext.newInstance(WwePlayers.class);
			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			WwePlayers player = (WwePlayers) jaxbUnmarshaller.unmarshal(file);
			System.out.println(player);
		} catch (JAXBException e) {
			e.printStackTrace();
		}
	}

}
