package com.deekshith.model;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@SuppressWarnings({ "restriction" })
@XmlRootElement
public class WwePlayers {
	private String name;
	private Double biceps;
	private float chest;
	private Double height;
	private int rank;
	List<String> titles;
	WwePlayerInfo playerInfo;

	@XmlElement
	public void setName(String name) {
		this.name = name;
	}

	@XmlElement
	public void setBiceps(Double biceps) {
		this.biceps = biceps;
	}

	@XmlElement
	public void setChest(float chest) {
		this.chest = chest;
	}

	@XmlElement
	public void setHeight(Double height) {
		this.height = height;
	}

	@XmlElement
	public void setRank(int rank) {
		this.rank = rank;
	}

	public WwePlayers(String name, Double biceps, float chest, Double height, List<String> titles, int rank,
			WwePlayerInfo playerInfo) {
		super();
		this.name = name;
		this.biceps = biceps;
		this.chest = chest;
		this.height = height;
		this.titles = titles;
		this.rank = rank;
		this.playerInfo = playerInfo;
	}

	public WwePlayers() {
	}

	public String getName() {
		return name;
	}

	public Double getBiceps() {
		return biceps;
	}

	public float getChest() {
		return chest;
	}

	public Double getHeight() {
		return height;
	}

	public int getRank() {
		return rank;
	}

	@XmlElement
	public void setTitles(List<String> titles) {
		this.titles = titles;
	}

	public List<String> getTitles() {
		return titles;
	}

	public WwePlayerInfo getPlayerInfo() {
		return playerInfo;
	}

	@XmlElement
	public void setPlayerInfo(WwePlayerInfo playerInfo) {
		this.playerInfo = playerInfo;
	}

	@Override
	public String toString() {
		return "WwePlayers [name=" + name + ", biceps=" + biceps + ", chest=" + chest + ", height=" + height
				+ ", titles=" + titles + "]";
	}

}
