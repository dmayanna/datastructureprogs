package com.deekshith.model;

import java.io.Serializable;
import java.util.List;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@SuppressWarnings({ "restriction", "unused", "serial" })
@XmlRootElement
public class WwePlayerInfo implements Serializable {
private String stageName;
private String realName;
private float phoneNumber;
private List<String> moviesActed;

public WwePlayerInfo(String stageName, String realName, int phoneNumber, List<String> moviesActed) {
	super();
	this.stageName = stageName;
	this.realName = realName;
	this.phoneNumber = phoneNumber;
	this.moviesActed = moviesActed;
}
public WwePlayerInfo() {
}

public String getStageName() {
	return stageName;
}
@XmlElement
public void setStageName(String stageName) {
	this.stageName = stageName;
}
public String getRealName() {
	return realName;
}
@XmlElement
public void setRealName(String realName) {
	this.realName = realName;
}
public float getPhoneNumber() {
	return phoneNumber;
}

@XmlElement
public void setPhoneNumber(int phoneNumber) {
	this.phoneNumber = phoneNumber;
}
public List<String> getMoviesActed() {
	return moviesActed;
}
@XmlElement
public void setMoviesActed(List<String> moviesActed) {
	this.moviesActed = moviesActed;
}
 
}
