package com.deekshith.staticmethod;
/**
 * 
 * @author deekshithmayanna
 * Static method demonstration
 * static method in a class does not require an object to be created to access the method
 *
 */

public class StaticMethodDemonstaration {
	
	public static void staticMethod()
	{
		System.out.println("I am a static Method");
	}

	public void nonStaticMethod()
	{
		System.out.println("I am a non Static method, Non Static methods require a object to call them");
		
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		staticMethod();
		StaticMethodDemonstaration demo = new StaticMethodDemonstaration();
		demo.nonStaticMethod();
		demo.staticMethod();
	}

}
