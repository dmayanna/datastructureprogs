package com.deekshith.operators;

import java.util.Scanner;

public class CalculatorArithemeticSwitch {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
        System.out.println("Enter the 2 Numbers");
		int num1 = input.nextInt();
		int num2 = input.nextInt();
		int result = 0;
		int i = 0;
		System.out.println("Enter your Choice ");
		System.out.println("Please note");
		System.out.println("Type 1: Addition, 2: Subtraction, 3: Multiplication, 4: Division");

		i = input.nextInt();

		switch (i) {
		case 1: {

			result = num1 + num2;
			break;
		}

		case 2: {
			result = num2 - num1;
			break;

		}

		case 3: {
			result = num1 * num2;
			break;

		}

		case 4: {
			result = num2 / num1;
			break;
		}
		
		case 5:{
			result = num2%num1;
			break;
		}

		default: {
			System.out.println("wrong choice");
		}

		}

		System.out.println("The result of the operation is " + result);

	}

}
