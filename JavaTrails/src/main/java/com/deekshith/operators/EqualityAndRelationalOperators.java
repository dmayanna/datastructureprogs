package com.deekshith.operators;

import java.util.Random;

/**
 * 
 * @author deekshithmayanna
 *==      equal to
!=      not equal to
>       greater than
>=      greater than or equal to
<       less than
<=      less than or equal to

 */

public class EqualityAndRelationalOperators {
	
	public static void main(String[] args)
	{
		Random randomGenerator = new Random();
		long num1=randomGenerator.nextInt(10);
		long num2=randomGenerator.nextInt(10);
		int num1Grt=0;
		int num2Grt=0;
		int notEqual=0;
		// generating random numbers
		
		
		System.out.println("Initial Number 1 is :"+num1);
		System.out.println("Initial Number 2 is :"+num2);
		
		while(num1!=num2)
		{
			num1=randomGenerator.nextInt(1000000000);
			num2=randomGenerator.nextInt(1000000000);
			notEqual++;
			if(num1>num2)
			{
				num1Grt++;
			}
			else 
			{
				num2Grt++;
			}
			
		}
		System.out.println("Final Number 1 is :"+num1);
		System.out.println("Final Number 2 is :"+num2);
		
		System.out.println("The number of time iterations occured before both numbers are equal "+notEqual);
		System.out.println("num1>num2 : "+num1Grt);
		System.out.println("Num2>num1 : "+num2Grt);
			
		
		
		
	}

}
