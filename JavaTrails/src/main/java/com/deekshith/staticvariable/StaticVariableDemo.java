package com.deekshith.staticvariable;

public class StaticVariableDemo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub	
		Student Deekshith = new Student("Deekshith");
		Student Krupesh = new Student("Krupesh");	
		System.out.println("Student Name = "+Deekshith.name);
		System.out.println("Student School Name = "+Deekshith.SchoolName);
		System.out.println("Student Name = "+Krupesh.name);
		System.out.println("Krupesh rollNo = "+Krupesh.SchoolName);
		System.out.println("Is Deekshith an instance of : "+(Deekshith instanceof Student));
	}

}
