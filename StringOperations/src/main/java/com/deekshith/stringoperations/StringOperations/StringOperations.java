package com.deekshith.stringoperations.StringOperations;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 * find string length string concatenation find index of a character in a given
 * string (if "diwakar" is given string let me know the position of letter a)
 * program for : string replace ,string replace all, substring string formatting
 * (upper case to lower case and vise versa) comparison of two string finding a
 * character sequence in a string( if given string is "diwakar", if i ask you to
 * find character sequence 'ar' yout program should print true if sequence is
 * available or false if not found) for above character sequence problem write
 * two sub programs 1> program is ok any case of character(upper case or lower
 * case) 2> program is strict on case match for character sequence( i.e. 'ar' is
 * a match but 'AR' is not a match) take a paragraph of text from a new article
 * and find the number of times following worlds are used "the", "of", "and"
 * (program can be case insensitive)
 *
 * 
 */
public class StringOperations {
	public static void main(String[] args) {

		String name = "Deekshith is a bad ass";

		System.out.println("Length of the string 'name' " + name.length());
		for (int i = 0; i < 2; i++) {
			name = name + " oh bad..!!! ";
		}
		System.out.println("The String after concatenation is :- " + name);
		System.out.println("Index of 'D' in the String 'name' is "+ name.indexOf('e'));
		// replace all
		System.out.println("original String before using 'replace all' : "+ name);
		System.out.println("String after replacing bad with good using 'replace all' : "+ name.replaceAll("bad", "good"));
		System.out.println("String after replacing bad with good using 'replace : "+name.replaceFirst("good", "bad"));
		System.out.println("String name in UPPERCASE : "+name.toUpperCase());
		System.out.println("String name in lowercase : "+name.toLowerCase());
		System.out.println("Does the String name contains the sequence 'eek' ?? :- "+(name.contains("eek")));
		System.out.println("Does the String name contains the sequence 'EEK' ?? :- "+(name.contains("EEK")));
		System.out.println("Does the String name contains the sequence eesdsgfdk ?? :- "+(name.contains("eesdsgfdk")));
		System.out.println("Does the String name contains the sequence 'EEK' ?? :- "+(name.toLowerCase().contains("EEK".toLowerCase())));

		File textfile = new File("1k.txt");
		try {
			FileReader filereader = new FileReader(textfile);
			BufferedReader reader = new BufferedReader(filereader);
			String line = null;
			@SuppressWarnings("unused")
			int count=0,count1=0;
			
			try {
				while((line = reader.readLine()) != null )
				{
					if(line.toLowerCase().contains("my"))
					{
						count++;
					}
					if(line.toLowerCase().contains("the"))
					{
						count1++;
					}
				
				}
				System.out.println("The number of 'my' in the file is "+count);
				System.out.println("The number of 'the' in the file is "+count1);


			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
	}
}
