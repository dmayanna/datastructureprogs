package Arraylist3.ArralistExample3;

import java.util.ArrayList;
import java.util.List;
public class App 
{
    public static void main( String[] args )
    {
    	ArrayList<String> ary = new ArrayList<String>();
    	ary.add("Al1:E1");
    	ary.add("Al1:E2");
    	ary.add("Al1:E3");
    	ary.add("Al1:E4");
    	System.out.println("The first list is "+ary);

    	
    	ArrayList<String> arrayList2 = new ArrayList<String>();
    	arrayList2.add("Al2,E1");
    	arrayList2.add("Al2,E2");
    	arrayList2.add("Al2,E3");
    	arrayList2.add("Al2,E4");
    	System.out.println("The first list is "+arrayList2);
    	
    	ArrayList<String> al = new ArrayList<String>();
    	al.addAll(arrayList2);
    	al.addAll(ary);
    	
    	System.out.println("The combined list is "+al);
    	List<String> al2 = new ArrayList<String>();
    	al2 = al.subList(1, 2);
    	System.out.println("The combined list is "+al2);


    	
    }
}
