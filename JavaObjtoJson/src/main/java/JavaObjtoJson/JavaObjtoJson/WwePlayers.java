package JavaObjtoJson.JavaObjtoJson;

import java.util.List;

public class WwePlayers {
	private String name;

	public WwePlayers(String name, Double biceps, Double chest, Double height,List<String> titles) {
		super();
		this.name = name;
		this.biceps = biceps;
		this.chest = chest;
		this.height = height;
		this.titles = titles;

	}
	public WwePlayers(){}

	public String getName() {
		return name;
	}

	public Double getBiceps() {
		return biceps;
	}

	public Double getchest() {
		return chest;
	}

	public Double getHeight() {
		return height;
	}

	private Double biceps;
	private Double chest;
	private Double height;

	public void setTitles(List<String> titles) {
		this.titles = titles;
	}

	public List<String> getTitles() {
		return titles;
	}

	List<String> titles;

	@Override
	public String toString() {
		return "WwePlayers [name=" + name + ", biceps=" + biceps + ", chest=" + chest + ", height=" + height
				+ ", titles=" + titles + "]";
	}
	

}
