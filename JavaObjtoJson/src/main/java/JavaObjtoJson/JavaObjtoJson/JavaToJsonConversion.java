package JavaObjtoJson.JavaObjtoJson;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class JavaToJsonConversion {
	@SuppressWarnings("unchecked")
	public static void main(String[] args) {
		List titles = new ArrayList<String>();
		titles.add("Intercontinental champion");
		titles.add("Royal Rumble Champion");
		titles.add("King of the Ring");
		WwePlayers rock = new WwePlayers("The Rock", 22.0, 52.0, 6.2,titles);
	

		ObjectMapper mapper = new ObjectMapper();
		try {
			String jsonInString = mapper.writeValueAsString(rock);
			System.out.println(jsonInString);
			mapper.writeValue(new File("playerJson.json"), rock);

		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
