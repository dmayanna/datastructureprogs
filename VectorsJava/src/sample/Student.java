package sample;

public class Student {
	/**
	 * 
	 */
	private String name;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDepartment() {
		return Department;
	}
	public void setDepartment(String department) {
		Department = department;
	}
	public String getGpa() {
		return Gpa;
	}
	public void setGpa(String gpa) {
		Gpa = gpa;
	}
	private String Department;
	private String Gpa;
	
	public Student(String name2, String department2, String gpa2) {
		// TODO Auto-generated constructor stub
		super();
		this.name = name2;
		Department = department2;
		Gpa = gpa2;
	}
	@Override
	public String toString() {
		return "Student [name=" + name + ", Department=" + Department + ", Gpa=" + Gpa + "]";
	}

}
