package VectorProgs;

import java.util.Iterator;
import java.util.ListIterator;
import java.util.Vector;

import sample.Student;

public class StudentVector {

	public static void main(String[] args) {
		
		Student one= new Student("Deekshith", "Electrical", "3.3");
		Student two= new Student("suresh", "CS", "4.3");

		Student three= new Student("mahesh", "Mech", "1.3");
		Student four= new Student("lokesh", "chemical", "3.8");

		System.out.println(one.toString());
		System.out.println(two.toString());
		System.out.println(three.toString());
		Vector<Student> StudentVector = new Vector<Student>();
		StudentVector.addElement(one);
		StudentVector.add(three);
		StudentVector.add(four);
		StudentVector.add(two);
		
		Vector<Integer> numbers = new Vector<Integer>(); 
		numbers.add(1);
		numbers.add(2);
		numbers.add(3);
		numbers.add(4);
		numbers.add(5);
		numbers.add(6);
		numbers.add(7);
		numbers.add(8);
		
		Iterator<Integer> it = numbers.iterator();
		ListIterator<Integer> iti = numbers.listIterator();
		
		while(it.hasNext())
		{
			System.out.println(it.next());
		}
		

		while(iti.hasPrevious())
		{
			System.out.println(iti.previous());
		}

		System.out.println(StudentVector);
		
		for(Student stew:StudentVector)
		{
			System.out.println("Name : "+stew.getName());
			System.out.println("Department : "+stew.getDepartment());
			System.out.println("Gpa : "+stew.getGpa());
			System.out.println("---------------------------------------------------------");
			
			
		}

	}

}
