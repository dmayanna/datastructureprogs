package VectorProgs;

import java.util.ArrayList;
import java.util.Vector;

public class VectorConstructor {

	public static void main(String[] args) {
		ArrayList<String> names = new ArrayList<String>();
		names.add("Deeky");
		names.add("rocky");
		
		// adding an arraylist to a vector 
		Vector<String> collectionholder = new Vector<String>(names);
        System.out.println("Vector Output : "+collectionholder);	
        System.out.println("Arraylist Output : "+names);
        // adding an arraylist and a vector to another arraylist
        ArrayList<String> collec = new ArrayList<String>();
        collec.addAll(names);
        collec.addAll(collectionholder);
        System.out.println("Arraylist of collections"+collec);
        ArrayList<Integer> intarraylist = new ArrayList<Integer>();
        intarraylist.add(12);
        intarraylist.add(45);
        
        for(int i:intarraylist)
        {
        	System.out.println("printing numbers in the integer arraylist "+i);
        }
        
        
        ArrayList<Object> oj = new ArrayList<Object>();
        oj.add(names);
        oj.add(collectionholder);
        oj.add(intarraylist);
        
        ArrayList<Integer> intArraylist = (ArrayList<Integer>) oj.get(2);
        
        System.out.println(" contents of the int arraylist "+intArraylist.get(0));
        
        System.out.println("arraylist of type object"+oj);
        
        
        Integer inty = new Integer(33);
        System.out.println("integer object value is "+ inty );
        int k=33;
        if(inty.intValue()==k)
        {
        	System.out.println("comparing value of integer object with an int value .. aaanddd they are equal");
        }
        
        Vector<Object> vecobj = new Vector<Object>();
        vecobj.add(oj);
        
        System.out.println(oj);
        
        Vector<Integer> inteswar = new Vector<>();
        inteswar.add(90);
        inteswar.add(54);
        inteswar.add(12);
        
        Vector<Integer> vecint = new Vector<Integer>();
        vecint.addAll(intarraylist);
        vecint.addAll(inteswar);
        vecint.addAll(intarraylist);
        vecint.addAll(inteswar);
        vecint.addAll(intarraylist);
        vecint.addAll(inteswar);
        vecint.addAll(intarraylist);
        vecint.addAll(inteswar);
        vecint.add(45);
        vecint.addAll(intarraylist);
        vecint.addAll(inteswar);
        vecint.addAll(intarraylist);
        vecint.addAll(inteswar);
        vecint.addAll(intarraylist);
        vecint.addAll(inteswar);
        vecint.addAll(intarraylist);
        vecint.addAll(inteswar);
        
        
        System.out.println("Vector int "+vecint);
        for(int s:vecint)
        {
        	System.out.println("vecint : "+s);
        }
        
        System.out.println("Size :"+vecint.size()+" Element at : "+vecint.elementAt(0)+" Last Element :"+vecint.lastElement()+" vecint capacity : "+vecint.capacity()+" index of vecint object at location 0 "+vecint.indexOf(inteswar, 1)+" "+vecint.clone());

	}
         
 }
