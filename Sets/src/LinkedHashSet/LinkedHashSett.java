package LinkedHashSet;

import java.util.LinkedHashSet;
import java.util.TreeSet;

public class LinkedHashSett {

	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub

		LinkedHashSet<String> linkedHashSet = new LinkedHashSet<String>();
		linkedHashSet.add("sd0");
		linkedHashSet.add("we");
		linkedHashSet.add("sdfd3");
		linkedHashSet.add("fg");
		linkedHashSet.add("gh");
		linkedHashSet.add("nm");
		linkedHashSet.add("mgf");
		linkedHashSet.add("jk");
		linkedHashSet.add("ui");
		
		TreeSet<String> treeSet = new TreeSet<>();
		treeSet.add("sd0");
		treeSet.add("sd1");
		treeSet.add("sd3");
		treeSet.add("sd2");
		treeSet.add("sd4");
		treeSet.add("sd6");
		treeSet.add("sd5");
		treeSet.add("sd8");
		treeSet.add("sd7");
		treeSet.addAll(linkedHashSet);
		treeSet.removeAll(linkedHashSet);
		System.out.println(" LinkedHashSet output "+linkedHashSet);
		System.out.println("TreeSet Output : "+treeSet);

	}

}
