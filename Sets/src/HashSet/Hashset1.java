package HashSet;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;

public class Hashset1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		HashSet<String> hashset = new HashSet<String>();
		hashset.add("Deekshith");
		hashset.add("Deekshith1");
		hashset.add("Deekshith");
		hashset.add("Deekshith2");
		hashset.add("Deekshith3");
		hashset.add("Deekshith4");
		hashset.add("Deekshith5");
		hashset.add("Deekshith");
		hashset.add("Deekshith");
		hashset.add("Deekshith");
		hashset.add("Deekshith");
		@SuppressWarnings("unchecked")
		HashSet<String> hashsetclone = (HashSet<String>) hashset.clone();
		hashset.add("Deesit");
		System.out.println("Original Hashset :"+hashset);
		System.out.println("Cloned Hashset :"+hashsetclone);
		System.out.println("Does the hashset contain Deekshith : "+hashset.contains("Deekshith"));
		System.out.println("Is hashset empty : "+hashset.isEmpty());
		
		hashsetclone.clear();
		System.out.println("Hashsetclone : "+hashsetclone);

		System.out.println("Is hashsetclone empty : "+hashsetclone.isEmpty());
		hashset.remove("Deekshith");
		for(String it:hashset)
		{
			System.out.println(" "+it);
	    }

		System.out.println(" Using Iterator ");
		Iterator<String> it = hashset.iterator();
		while(it.hasNext())
		{
		System.out.println(it.next());
		     }
		
		ArrayList<String> Arraylist = new ArrayList<String>(hashset);
		
		System.out.println(" "+Arraylist.get((Arraylist.size()-1)));
}}
