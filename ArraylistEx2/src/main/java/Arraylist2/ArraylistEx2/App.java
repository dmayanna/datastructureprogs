package Arraylist2.ArraylistEx2;

import java.util.ArrayList;
import java.util.List;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
      ArrayList<String> a1 = new ArrayList<String>();
      
      a1.add("Steve");
      a1.add("Justin");
      a1.add("Ajeet");
      a1.add("John");
      a1.add("Arnold");
      a1.add("Chaitanya");
      System.out.println("Original arraylist "+a1);      
      ArrayList<String> al2 = new ArrayList<String>(a1.subList(1,4));
      System.out.println("Sublist stored in Arraylist :"+al2);
      List<String> list = a1.subList(1,4);
      System.out.println("Subject stored in List "+list);
      
    }
}
