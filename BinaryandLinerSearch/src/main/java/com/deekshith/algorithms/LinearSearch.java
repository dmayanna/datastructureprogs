package com.deekshith.algorithms;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Random;
import java.util.TreeSet;

public class LinearSearch {

	public static void main(String[] args) {

		LinkedHashSet<Integer> container = new LinkedHashSet<Integer>();
		TreeSet<Integer> sortedContainer = new TreeSet<Integer>();
		int i =0;
	    Random randomGenerator = new Random();

		while(container.size()<1000)
		{
		      int randomInt = randomGenerator.nextInt(1000);
		      container.add(randomInt);
		}
		while(sortedContainer.size()<1000)
		{
		      int randomInt = randomGenerator.nextInt(1000);
		      sortedContainer.add(randomInt);
		}
		System.out.println("Sorted : "+sortedContainer);
		System.out.println("Unsorted : "+container);
		System.out.println(container.size());
		
		   Integer unsortedList[] = new Integer[container.size()];
		   unsortedList = container.toArray(unsortedList);
		   
		   Integer sortedList[] = new Integer[container.size()];
		   sortedList = sortedContainer.toArray(sortedList);

		// Liner search on unsorted data 
		double time =  System.nanoTime();
		for(int k=0; k<1000;k++)
		{
			if(unsortedList[k]==453)
			{
				System.out.println("The time required to find the number 453 in unsorted list is "+ (System.nanoTime() -time));
			}
		}
		 time =  System.nanoTime();
		for(int k=0; k<1000;k++)
		{
			if(sortedList[k]==453)
			{
				System.out.println("The time required to find the number 453 in sorted list is "+ (System.nanoTime() -time));
			}
		}
	
		
	}

}
