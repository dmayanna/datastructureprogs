package com.deekshith.FileReading.SimpleInterest;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Scanner;

public class SimpleInterest {
	public static void main(String[] args) throws ParseException, IOException {

		FileReader reader = new FileReader("MoneyGiven.txt");
		BufferedReader bufferedReader = new BufferedReader(reader);
		String currentLine = "";
		ArrayList<String> StringList = new ArrayList<String>();
		ArrayList<String> date = new ArrayList<String>();
		ArrayList<Double> principal = new ArrayList<Double>();
		int dollarIndex = 0;
		ArrayList<Double> indianCurrency = new ArrayList<Double>();
		String valueinrupee = null;
		System.out.println("Please Enter the Rate of interest");
		Scanner scan = new Scanner(System.in);
		double rateOfInterest = scan.nextDouble();
		ArrayList<Integer> diffInDaysList = new ArrayList<Integer>();

		while ((currentLine = bufferedReader.readLine()) != null)

		{

			StringList.add(currentLine);

		}

		for (int i = 0; i <= StringList.size(); i = i + 11) {

			if (StringList.get(i).length() == 5) {
				date.add((StringList.get(i).subSequence(3, 5)) + "-" + StringList.get(i).substring(0, 3) + "-"
						+ StringList.get(i + 1).trim());
			} else if (StringList.get(i).length() == 4) {
				date.add(("0" + StringList.get(i).substring(3, 4) + "-" + StringList.get(i).subSequence(0, 3)) + "-"
						+ StringList.get(i + 1).trim());
			}

			dollarIndex = (StringList.get(i + 3).indexOf("$"));
			principal.add(Double.parseDouble((StringList.get(i + 3).substring(dollarIndex + 1))));

			if (StringList.get(i + 5).indexOf("R") == StringList.get(i + 5).indexOf("@") + 2) {
				for (String rupeeLine : StringList.get(i + 5).split(" ")) {
					if (rupeeLine.contains("Rs")
							&& StringList.get(i + 5).indexOf("R") == StringList.get(i + 5).indexOf("@") + 2) {
						valueinrupee = rupeeLine.substring(2);
					}

				}
			} else {
				valueinrupee = "63.00";
			}

			indianCurrency.add(Double.parseDouble(valueinrupee));

		}
		long diff = 0;
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");
		int diffDays = 0;
		Date currentdate = new Date();
		Date latestLoanDate = dateFormat.parse(date.get(0));
		diff = currentdate.getTime() - latestLoanDate.getTime();
		diffDays = (int) (diff / (24 * 60 * 60 * 1000));
		diffInDaysList.add(diffDays);

		for (int i = 0; i < (principal.size() - 1); i++) {
			String strDate = date.get(i);
			String strDate1 = date.get(i + 1);
			Date varDate = dateFormat.parse(strDate);
			Date varDate1 = dateFormat.parse(strDate1);
			diff = varDate.getTime() - varDate1.getTime();
			diffDays = (int) (diff / (24 * 60 * 60 * 1000));
			diffInDaysList.add(diffDays);
		}
		double simpleInterest = 0.0;
		double totalSimpleInterest = 0.0;

		int counter = diffInDaysList.size();
		double TotalPrincipal =0.0;
		for (int i = 0; i < counter; i++) {
			System.out.println("*********************************************************************************");
			TotalPrincipal=principal.get(counter - (i + 1))+TotalPrincipal;
			System.out.println("Date of Trannsaction : " + date.get(counter - (i + 1)));
			System.out.println("Amount lent : $" + principal.get(counter - (i + 1))+" Rs"+(principal.get(counter - (i + 1)) * indianCurrency.get(counter - (i + 1))));
			System.out.println("Total Principal Amount : $"+TotalPrincipal);
		
			System.out.println("Duration of days : " + diffInDaysList.get(counter - (i + 1)));
			
//			System.out.println("1 : " + principal.get(counter - (i + 1)));
//			System.out.println("2 : " + principal.get(counter - (i + 1)) * indianCurrency.get(counter - (i + 1)));
//			System.out.println("3 : " + diffInDaysList.get(counter - (i + 1)));
			double time = ((double) diffInDaysList.get(counter - (i + 1)) / (double) 364);
			double rateOfInterest1 = (double) rateOfInterest / (double) 100;
//			System.out.println("time 2 : " + time);
			simpleInterest =(TotalPrincipal * indianCurrency.get(counter - (i + 1)))* (time) * rateOfInterest1;
			System.out.println("Annual rate of interest : "+rateOfInterest);
			System.out.println("Simple Interest : Rs" +simpleInterest);
			totalSimpleInterest = simpleInterest+totalSimpleInterest;
			
		}
		System.out.println("**********************************************************************");
		System.out.println("Total Principal amount $"+TotalPrincipal);
		System.out.println("Total Simple Interest Rs"+totalSimpleInterest);

	}
}
